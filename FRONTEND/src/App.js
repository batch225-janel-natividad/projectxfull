import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import { useState, useEffect } from 'react';
import Navbar from './components/AppNavbar';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Shop from './pages/Shop';
import Logout from './pages/Logout';
import { UserProvider } from './UserContext';
import Admin from './pages/Administrator';
import About from './pages/About';
import AdminNavbar from './components/AdminNavbar';
import Cart from './pages/Cart';
import UpdateProduct from './pages/UpdateProduct';


function JanelShop() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/user/userDetails`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(res => res.json())
      .then(data => {

        console.log(typeof data._id)
        if (typeof data._id !== "undefined") {

          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          });


        } else {

          setUser({
            id: null,
            isAdmin: null
          });

        }

      })

  }, []);

  const userLoggedin = (user._id !== null)
  const userIsAdmin = (user.isAdmin)

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        {(userLoggedin && userIsAdmin) ?
          <AdminNavbar />
          :
          <Navbar />
        }
        <Routes>
          <Route exact path="/" element={<Home />} ></Route>
          <Route path="/shop" element={<Shop />} ></Route>
          <Route path="/register" element={<Register />} ></Route>
          <Route path="/cart" element={<Cart />}></Route>
          <Route path="/login" element={<Login />} ></Route>
          <Route path="/logout" element={<Logout />} ></Route>
          <Route path="/updateProduct/:productId" element={<UpdateProduct />} />
          <Route path="/about" element={<About />} ></Route>

          {(userLoggedin && userIsAdmin) ?
            <Route path="/admin" element={<Admin />}></Route>
            :
            null
          }

        </Routes>
      </Router>
    </UserProvider>
  );
}


export default JanelShop;
