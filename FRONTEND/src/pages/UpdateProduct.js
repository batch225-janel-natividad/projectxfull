import { useState, useEffect } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import Swal from 'sweetalert2'


// updating products
export default function UpdateProduct(props) {

    const { productId } = useParams()

    const [productName, setName] = useState('');
    const [productPrice, setPrice] = useState('');
    const [productDescription, setDescription] = useState('');
    const [productCategories, setCategories] = useState('');
    const [productColor, setColor] = useState('');
    const [productSize, setSize] = useState('');
    console.log("Hello World");
    // For determining if button is disabled or not
    const [isActive, setIsActive] = useState(false)

    const navigate = useNavigate()

    // get product details and set to state then edit and update on submit
    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/product/${productId}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        })
            .then(response => response.json())
            .then(result => {
                console.log(result)
                setName(result.name)
                setDescription(result.description)
                setPrice(result.price)
                setCategories(result.categories)
                setSize(result.size)
                setColor(result.color)
            })
            .catch(console.error)
    }, [])
    function updateProduct(event) {
        event.preventDefault()
        fetch(`${process.env.REACT_APP_API_URL}/product/update/${productId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: productName,
                description: productDescription,
                categories: productCategories,
                color: productColor,
                size: productSize,
                price: productPrice,
            })
        })
            .then(response => response.json())
            .then(result => {

                if (result) {
                    Swal.fire({
                        title: 'Success!',
                        text: 'Product updated successfully',
                        icon: 'success',
                        confirmButtonText: 'OK'
                    })
                    navigate('/admin')

                } else {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: 'Error updating product'
                    })
                }
            })
            .catch(console.error)
    }

    return (
        <div className="flex items-center justify-center h-screen">
            <div className="w-full p-6 m-auto mt-20 bg-red-100 rounded-md shadow-xl shadow-pink-600/40 ring ring-2 ring-pink-600 lg:max-w-xl">
                <h1 className="text-2xl font-bold text-center mb-4">Update Product</h1>
                <form onSubmit={updateProduct}>
                    <div className="mb-4">
                        <label htmlFor="name" className="block font-medium">Name</label>
                        <input
                            id="name"
                            type="text"
                            className="form-input mt-1 block w-full"
                            placeholder="Enter name"
                            value={productName}
                            onChange={(event) => setName(event.target.value)}
                        />
                    </div>
                    <div className="mb-4">
                        <label htmlFor="description" className="block font-medium">Description</label>
                        <input
                            id="description"
                            type="text"
                            className="form-input mt-1 block w-full"
                            placeholder="Enter description"
                            value={productDescription}
                            onChange={(event) => setDescription(event.target.value)}
                        />
                    </div>
                    <div className="mb-4">
                        <label htmlFor="categories" className="block font-medium">Categories</label>
                        <input
                            id="categories"
                            type="text"
                            className="form-input mt-1 block w-full"
                            placeholder="Enter categories"
                            value={productCategories}
                            onChange={(event) => setCategories(event.target.value)}
                        />
                    </div>
                    <div className="mb-4">
                        <label htmlFor="color" className="block font-medium">Color</label>
                        <input
                            id="color"
                            type="text"
                            className="form-input mt-1 block w-full"
                            placeholder="Enter color"
                            value={productColor}
                            onChange={(event) => setColor(event.target.value)}
                        />
                    </div>
                    <div className="mb-4">
                        <label htmlFor="size" className="block font-medium">Size</label>
                        <input
                            id="size"
                            type="text"
                            className="form-input mt-1 block w-full"
                            placeholder="Enter size"
                            value={productSize}
                            onChange={(event) => setSize(event.target.value)}
                        />
                    </div>
                    <div className="mb-4">
                        <label htmlFor="price" className="block font-medium font-bold">Price</label>
                        <input
                            id="price"
                            type="text"
                            className="form-input mt-1 block w-full"
                            placeholder="Enter price"
                            value={productPrice}
                            onChange={(event) => setPrice(event.target.value)}
                        />
                    </div>
                    <button
                        type="submit"
                        disabled={isActive}
                        className="w-full bg-pink-500 hover:bg-pink-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                    >
                        Update Product
                    </button>
                </form>
            </div>
        </div>

    )
}

