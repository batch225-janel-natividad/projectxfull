import React, { useState, useEffect, useContext } from "react";
import CreateProduct from "../components/CreateProduct";
import UserContext from "../UserContext";
import Modal from 'react-modal';
import {Link} from 'react-router-dom';
Modal.setAppElement('#root');

function Admin() {
  const [products, setProducts] = useState([]);
  const { user } = useContext(UserContext);


  //RETRIVE ALL PRODUCT
  useEffect(() => {
    const token = localStorage.getItem('token')

    fetch(`${process.env.REACT_APP_API_URL}/product/retriveAllProduct`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      }
    })
      .then((response) => response.json())
      .then((data) => {
        console.log("Success:", data);
        setProducts(data);
      })
      .catch((error) => {
        console.error("Error:", error);
      });

  }, [user]);

  //CREATE PRODUCT
  const [isModalOpen, setIsModalOpen] = useState(false);
  const handleOpenModal = () => {
    setIsModalOpen(true);
  };
  const handleCloseModal = () => {
    setIsModalOpen(false);
    window.location.reload();
  };
  //TOGGLE ACTIVE/INACTIVE
  const toggleProductActive = (productId) => {
    const token = localStorage.getItem('token');

    // Check the current value of the isActive property for the product
    const product = products.find(product => product._id === productId);
    const isActive = product.isActive;

    // Call the appropriate API endpoint and update the isActive property based on its current value
    if (isActive) {
      fetch(`${process.env.REACT_APP_API_URL}/product/archive/${productId}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`
        }
      })
        .then((response) => {
          if (!response.ok) {
            throw new Error('Network response was not ok');
          }
          return response.json();
        })
        .then((data) => {
          console.log('Success:', data);
          // Update the isActive property in the products state
          const updatedProducts = products.map(product => {
            if (product._id === productId) {
              return {
                ...product,
                isActive: false
              };
            }
            return product;
          });
          setProducts(updatedProducts);
        })
        .catch((error) => {
          console.error('Error:', error);
        });
    } else {
      fetch(`${process.env.REACT_APP_API_URL}/product/activate/${productId}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`
        }
      })
        .then((response) => {
          if (!response.ok) {
            throw new Error('Network response was not ok');
          }
          return response.json();
        })
        .then((data) => {
          console.log('Success:', data);
          // Update the isActive property in the products state
          const updatedProducts = products.map(product => {
            if (product._id === productId) {
              return {
                ...product,
                isActive: true
              };
            }
            return product;
          });
          setProducts(updatedProducts);
        })
        .catch((error) => {
          console.error('Error:', error);
        });
    }
  };
  //EDIT PRODUCT

  return (
    <div className="max-w-full sm:-mx-6 mt-12 mx-auto block">
      <div className="flex justify-end">
        <button
          onClick={handleOpenModal}
          class="bg-rose-300 hover:bg-pink-500 text-white font-bold py-2 px-4 mb-5 rounded ml-auto">
          Create Product
        </button>
        <CreateProduct
          isOpen={isModalOpen}
          onClose={handleCloseModal} />
      </div>
      <div class="w-full">
        <div class="mx-auto max-w-7xl sm:px-6 lg:px-8 mt-10">
          <div class="border-b border-gray-200 sm:rounded-lg">
            <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
              <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                  <table class="min-w-full divide-y divide-gray-200">
                    <thead className="bg-gray-50">
                      <tr>
                        <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Name</th>
                        <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Description</th>
                        <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Categories</th>
                        <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Color</th>
                        <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Size</th>
                        <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Price</th>
                        <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">IsActive</th>
                        <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Actions</th>
                      </tr>
                    </thead>
                    <tbody className="bg-white divide-y divide-gray-200">
                      {products.map((product) => (
                        <tr key={product._id}>
                          <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">{product.name}</td>
                          <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500"><div class="truncate w-40">{product.description}</div></td>
                          <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">{product.categories.join(", ")}</td>
                          <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">{product.color}</td>
                          <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">{product.size}</td>
                          <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">{product.price}</td>
                          <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                            <label htmlFor={`toggle-${product._id}`} className="flex items-center cursor-pointer">
                              <div className="relative">
                                <input id={`toggle-${product._id}`} type="checkbox" className="hidden" onChange={() => toggleProductActive(product._id)} checked={product.isActive} />
                                <div className="toggle__line w-10 h-4 bg-gray-400 rounded-full shadow-inner"></div>
                              </div>
                              <div className="ml-3 text-gray-700 font-medium">{product.isActive ? "Active" : "Inactive"}</div>
                            </label>
                          </td>
                          <td className="px-6 py-4 whitespace-nowrap text-sm text-red-500">
                            <Link className="btn btn-primary" to={`/updateProduct/${product._id}`}>Update</Link>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}


export default Admin;
