import React, { useState, useEffect } from "react";
import Footer from "../components/Footer";
import CardProduct from "../components/CardProduct";

function Shop() {
  const [products, setProducts] = useState([]);
  const [error, setError] = useState(null);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/product/retrievAllActive`)
      .then((result) => {
        if (!result.ok) {
          throw new Error("Network response was not ok");
        }
        return result.json();
      })
      .then((result) => {
        console.log(result);
        setProducts(
          result.map((product) => <CardProduct key={product._id} product={product} />)
        );
      })
      .catch((error) => {
        console.error("Error fetching products:", error);
        setError(error);
      });
  }, []);

  if (error) {
    return <div>Error fetching products: {error.message}</div>;
  }

  return (
    <>
      {products.length > 0 && (
        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4">
          {products}
        </div>

      )}
      <Footer />
    </>
  );
}

export default Shop;
