import { useState, useEffect, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import Footer from '../components/Footer';

function Login() {
    const { user, setUser } = useContext(UserContext)
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [isActive, setIsActive] = useState(false)

    const retrieveUser = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/user/userDetails`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
            .then(response => response.json())
            .then(result => {


                setUser({
                    id: result._id,
                    isAdmin: result.isAdmin

                })
            })
    }

    function authenticate(event) {
        event.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/user/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
            .then(response => response.json())
            .then(result => {
                if (typeof result.access !== "undefined") {
                    localStorage.setItem('token', result.access)

                    retrieveUser(result.access)

                    Swal.fire({
                        title: 'Login Successful!',
                        icon: 'success',
                        text: 'Welcome to Janel Shop!' && 'Happy Shopping!'
                    })
                } else {
                    Swal.fire({
                        title: 'Authentication Failed!',
                        icon: 'error',
                        text: 'Invalid Email or password'
                    })
                }
            })
    }

    useEffect(() => {
        if ((email !== '' && password !== '')) {
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [email, password])

    return (user.id !== null) ?
        <Navigate to="/" />
        :
        (
            <div className="relative flex flex-col justify-center min-h-screen overflow-hidden">
                <div className="w-full p-6 m-auto mt-20 bg-red-100 rounded-md shadow-xl shadow-pink-600/40 ring ring-2 ring-pink-600 lg:max-w-xl">
                    <h1 className="text-3xl font-semibold text-center text-gray-700  uppercase">
                        LOGIN
                    </h1>
                    <form onSubmit={(event) => authenticate(event)} className="mt-2">
                        <div className="mb-6">
                            <label htmlFor="userEmail" className="block text-gray-700 font-bold mb-2">Email Address</label>
                            <input
                                id="userEmail"
                                type="email"
                                placeholder="Enter email"
                                value={email}
                                onChange={(event) => setEmail(event.target.value)}
                                required
                                className="appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                            />
                            <p className="text-gray-600 text-xs italic">We'll never share your email with anyone else.</p>
                        </div>

                        <div className="mb-4">
                            <label htmlFor="password" className="block text-gray-700 font-bold mb-2">Password</label>
                            <input
                                id="password"
                                type="password"
                                placeholder="Password"
                                value={password}
                                onChange={(event) => setPassword(event.target.value)}
                                required
                                className="appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                            />
                        </div>

                        {isActive ? (
                            <button type="submit" id="submitBtn" className="w-full bg-pink-500 hover:bg-pink-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">
                                LOGIN
                            </button>
                        ) : (
                            <button type="submit" id="submitBtn" className="w-full bg-pink-500 hover:bg-pink-700 text-white font-bold py-2 px-9 rounded opacity-50 cursor-not-allowed focus:outline-none focus:shadow-outline " disabled>
                                LOGIN
                            </button>
                        )}
                        <p className="mt-8 text-xs font-light text-center text-gray-700">
                            {" "}Don't have an account?{" "}
                            <a
                                href="/register"
                                className="font-medium text-pink-600 hover:underline">
                                Register
                            </a>
                        </p>
                    </form>
                </div>
                <Footer />
            </div>

        );
}

export default Login;