import { useState } from 'react';
import axios from 'axios';
import Swal from 'sweetalert2';

const Register = () => {
  const [formData, setFormData] = useState({
    email: '',
    password: '',
  });
  const [error, setError] = useState('');

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const response = await axios.post(`${process.env.REACT_APP_API_URL}/user/register`, formData);
      console.log(response.data);

      // Show SweetAlert notification on successful registration
      Swal.fire({
        icon: 'success',
        title: 'Success!',
        text: 'Your account has been created successfully.',
      }).then(() => {
        window.location.href = '/login'; // Redirect to login page
      });

    } catch (error) {
      console.error(error.response.data.message);
      setError(error.response.data.message);
    }
  };

  return (
    <form className="mx-auto max-w-md p-8 bg-red-100 rounded-lg shadow-lg shadow-pink-600/40 mt-20 ring ring-2 ring-pink-600">
      <h2 className="text-2xl font-medium mb-6 text-center">Create an Account</h2>
      {error && <div className="text-red-600 mb-4">{error}</div>}
      <div className="grid grid-cols-1 sm:grid-cols-2 gap-4 mb-4">
      </div>
      <div className="mb-4">
        <label className="block mb-1 font-medium text-gray-700">Email</label>
        <input
          type="email"
          name="email"
          value={formData.email}
          onChange={handleChange}
          className="w-full border border-gray-300 rounded py-2 px-3 focus:outline-none focus:border-blue-500"
          required
        />
      </div>
      <div className="mb-4">
        <label className="block mb-1 font-medium text-gray-700">Password</label>
        <input
          type="password"
          name="password"
          value={formData.password}
          onChange={handleChange}
          className="w-full border border-gray-300 rounded py-2 px-3 focus:outline-none focus:border-blue-500"
          required
        />
      </div>
      <button
        type="submit"
        className="w-full bg-pink-500 hover:bg-pink-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
        onClick={handleSubmit}
      >
        Register
      </button>
    </form>

  );
};

export default Register;
