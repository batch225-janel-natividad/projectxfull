import React from 'react'
import Footer from '../components/Footer';
const About = () => {
  return (
    <>
   
    <div>
    <h1 className='text-bold text-center text-pink-500 mt-10 mr-10 ml-10 mb-20'>ABOUT JANEL SHOP</h1>
      <p className='text-center mr-40 ml-40 mb-20'>Shopify is an example of a company that nails its About Us page design while doing a great job at telling the world its story that tells the world their story. Their page shows how important it is for them to talk about their mission to help people start, run, and grow a business. The company prides itself in making ecommerce easier for everyone. Also, it paints a picture of the future for Shopify by mentioning that they’re building a 100-year company by investing in their people and the planet.
      
      The New York Times shares a lot of information on its website about page, and this includes numbers relating to their staff and readership. It is important for them to emphasise to readers all around the world that they have a large team of journalists who write stories about countries they are local to. The fact that the publication had 7.5 million subscribers last year only adds to the trust they have, leading to more readers in the future.</p>
  <Footer/>
    </div>
    </>
  )
}

export default About
