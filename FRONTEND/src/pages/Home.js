import CarouselComponent from '../components/Carousel';
import Footer from '../components/Footer';

function Home () {
    return(
        <>
            <CarouselComponent/>          
            <Footer/>
        </>
    )
}

export default Home;