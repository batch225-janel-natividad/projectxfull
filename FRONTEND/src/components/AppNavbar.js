import React from 'react';
import { useState, useContext } from 'react';
import Logo from './images/Logo.png';
import UserContext from '../UserContext';


function Navbar() {
  const {user} = useContext(UserContext)
  const [isMobileMenuOpen, setIsMobileMenuOpen] = useState(false);

  const handleMobileMenuOpen = () => {
    setIsMobileMenuOpen(true);
  };

  const handleMobileMenuClose = () => {
    setIsMobileMenuOpen(false);
  };
 
  return (
      <nav className="bg-pink-200">
      <div className="max-w-7xl mx-auto px-8 sm:px-6 lg:px-8">
        <div className="relative flex items-center justify-between h-16">
          <div className="flex-shrink-0">
            <img src={Logo} className="h-18 w-12 ml-5" alt="Logo" />
          </div>
          <div className="hidden md:block">
            <div className="ml-4 flex items-center space-x-4">
              <a href="/" className="text-gray px-3 py-2 rounded-md text-sm font-medium transition duration-300 ease-in-out hover:bg-pink-300 hover:text-gray-900 transform hover:-translate-y-1 hover:scale-110">Home</a>
              <a href="/about" className="text-gray px-3 py-2 rounded-md text-sm font-medium transition duration-300 ease-in-out hover:bg-pink-300 hover:text-gray-900 transform hover:-translate-y-1 hover:scale-110">About</a>
              {	(user.id) ?
              <>
              <a href="/shop" className="text-gray px-3 py-2 rounded-md text-sm font-medium transition duration-300 ease-in-out hover:bg-pink-300 hover:text-gray-900 transform hover:-translate-y-1 hover:scale-110">Shop</a>
              <a href="/cart" className="text-gray px-3 py-2 rounded-md text-sm font-medium transition duration-300 ease-in-out hover:bg-pink-300 hover:text-gray-900 transform hover:-translate-y-1 hover:scale-110">Cart</a>
              <a href="/logout" className="text-gray px-3 py-2 rounded-md text-sm font-medium transition duration-300 ease-in-out hover:bg-pink-300 hover:text-gray-900 transform hover:-translate-y-1 hover:scale-110">Logout</a>
              </>
              :
              <>
              <a href="/register" className="text-gray px-3 py-2 rounded-md text-sm font-medium transition duration-300 ease-in-out hover:bg-pink-300 hover:text-gray-900 transform hover:-translate-y-1 hover:scale-110">Register</a>
              <a href="/login" className="text-gray px-3 py-2 rounded-md text-sm font-medium transition duration-300 ease-in-out hover:bg-pink-300 hover:text-gray-900 transform hover:-translate-y-1 hover:scale-110">Login</a>
              </>
              }
              
            </div>
          </div>
          <div className="md:hidden flex items-center">
            <button
              className="text-gray-800 hover:text-gray-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-pink-100 focus:ring-pink-300"
              onClick={handleMobileMenuOpen}>
              <svg className="h-6 w-6" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M4 6H20M4 12H20M4 18H20" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
              </svg>
            </button>
          </div>
        </div>
      </div>

      {/* Mobile menu */}
      {isMobileMenuOpen && (
        <div className="md:hidden" id="mobile-menu">
          <div className="px-2 pt-2 pb-3 space-y-1">
            <a href="/" className="text-gray-800 hover:text-gray-600 block px-3 py-2 text-base font-medium">Home</a>
            <a href="/about" className="text-gray-800 hover:text-gray-600 block px-3 py-2 text-base font-medium">About</a>
            { (user.id) ?
            <>
            <a href="/shop" className="text-gray-800 hover:text-gray-600 block px-3 py-2 text-base font-medium">Shop</a>
            <a href="/cart" className="text-gray-800 hover:text-gray-600 block px-3 py-2 text-base font-medium">Cart</a>
            <a href="/logout" className="text-gray-800 hover:text-gray-600 block px-3 py-2 text-base font-medium">Logout</a>
            </>
            :
						<>
            <a href="/register" className="text-gray-800 hover:text-gray-600 block px-3 py-2 text-base font-medium">Register</a>
            <a href="/login" className="text-gray-800 hover:text-gray-600 block px-3 py-2 text-base font-medium">Login</a>
            </>
            }
           
          </div>
          <button
            className="block w-full px-4 py-2 text-base font-medium text-center text-pink-600 bg-pink-50 hover:bg-pink-100 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-pink-100 focus:ring-pink-300"
            onClick={handleMobileMenuClose}
          >
            Close Menu
          </button>
        </div>
      )}
    </nav>
  );
}

export default Navbar;