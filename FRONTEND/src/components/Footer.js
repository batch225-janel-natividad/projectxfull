import React from 'react';
import Logo from "./images/Logo.png";

function Footer() {
  return (
    <div className="flex flex-col h-screen">
      <footer className="bg-pink-200 py-4 px-4 sm:px-6 lg:px-8 mt-auto">
        <div className="max-w-7xl mx-auto">
          <div className="flex justify-center">
            <img src={Logo} className="h-16 w-12" alt="Logo" />
          </div>
          <div className="flex justify-center mt-2">
            <p className="text-gray-800 text-sm">&copy; 2023 Janel Shop. All rights reserved.</p>
          </div>
        </div>
      </footer>
    </div>
  );
}

export default Footer;
