import React, { useState } from 'react';
import Modal from 'react-modal';
import Switch from 'react-switch';

Modal.setAppElement('#root');

const CreateProduct = ({ isOpen, onClose, onSubmit }) => {
    const [productName, setProductName] = useState('');
    const [productPrice, setProductPrice] = useState('');
    const [productDescription, setProductDescription] = useState('');
    const [productCategories, setProductCategories] = useState('');
    const [productColor, setProductColor] = useState('');
    const [productSize, setProductSize] = useState('');
    const [productIsActive, setProductIsActive] = useState(false);
  
    const handleFormSubmit = (event) => {
      event.preventDefault();
  
      const token = localStorage.getItem('token');
  
      const createProduct = async () => {
        try {
          const response = await fetch(`${process.env.REACT_APP_API_URL}/product/createProduct`, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${token}`,
            },
            body: JSON.stringify({
              name: productName,
              description: productDescription,
              categories: productCategories,
              color: productColor,
              size: productSize,
              price: productPrice,
              isActive: productIsActive,
            }),
          });
      
          if (!response.ok) {
            throw new Error('Network response was not ok');
          }
      
          const data = await response.json();
          console.log('Product created successfully:', data);
          onClose();
        } catch (error) {
          console.error('Error creating product:', error);
        }
      };
      
      createProduct();
    }
  
    const onProductNameChange = (event) => {
      setProductName(event.target.value);
    };
  
    const onProductPriceChange = (event) => {
      setProductPrice(event.target.value);
    };
  
    const onProductDescriptionChange = (event) => {
      setProductDescription(event.target.value);
    };
  
    const onProductCategoriesChange = (event) => {
      setProductCategories(event.target.value);
    };
  
    const onProductColorChange = (event) => {
      setProductColor(event.target.value);
    };
  
    const onProductSizeChange = (event) => {
      setProductSize(event.target.value);
    };
  
    const onProductIsActiveChange = (event) => {
      setProductIsActive(event.target.checked);
    };
  return (
    <Modal
        isOpen={isOpen}
        onRequestClose={onClose}
        className="bg-white rounded-lg p-4 absolute top-20 left-10 right-10 bottom-10 overflow-hidden"
        overlayClassName="fixed top-0 left-0 right-0 bottom-0 bg-black z-50"
    >
  <div className="max-h-full overflow-y-auto">
    <h2 className="text-lg font-bold mb-4">Create Product</h2>
      <form onSubmit={handleFormSubmit}>
        <div className="mb-4">
          <label htmlFor="product-name" className="block font-bold mb-2">
            Product Name
          </label>
          <input
            id="product-name"
            type="text"
            className="border-2 border-gray-400 rounded-lg py-2 px-4 w-full"
            value={productName}
            onChange={onProductNameChange}
            required
          />
        </div>
        <div className="mb-4">
          <label htmlFor="product-price" className="block font-bold mb-2">
            Product Price
          </label>
          <input
            id="product-price"
            type="number"
            className="border-2 border-gray-400 rounded-lg py-2 px-4 w-full"
            value={productPrice}
            onChange={onProductPriceChange}
            required
          />
        </div>
        <div className="mb-4">
          <label htmlFor="product-description" className="block font-bold mb-2">
            Product Description
          </label>
          <textarea
            id="product-description"
            className="border-2 border-gray-400 rounded-lg py-2 px-4 w-full"
            value={productDescription}
            onChange={onProductDescriptionChange}
            required
          />
          </div>
        <div className="mb-4">
          <label htmlFor="product-categories" className="block font-bold mb-2">
            Product Categories
          </label>
          <input
            id="product-categories"
            type="text"
            className="border-2 border-gray-400 rounded-lg py-2 px-4 w-full"
            value={productCategories}
            onChange={onProductCategoriesChange}
            required
            />
            </div>
            <div className="mb-4">
            <label htmlFor="product-color" className="block font-bold mb-2">
            Product Color
            </label>
            <input
                    id="product-color"
                    type="text"
                    className="border-2 border-gray-400 rounded-lg py-2 px-4 w-full"
                    value={productColor}
                    onChange={onProductColorChange}
                    required
                />
            </div>
            <div className="mb-4">
            <label htmlFor="product-size" className="block font-bold mb-2">
            Product Size
            </label>
            <input
                    id="product-size"
                    type="text"
                    className="border-2 border-gray-400 rounded-lg py-2 px-4 w-full"
                    value={productSize}
                    onChange={onProductSizeChange}
                    required
                />
            </div>
            <div className="mb-4">
            <label htmlFor="product-is-active" className="block font-bold mb-2">
                    Is Active
                    </label>
                    <Switch
                    id="product-is-active"
                    checked={Boolean(productIsActive)}
                    onChange={onProductIsActiveChange}
                    />
            </div>
            <div className="flex justify-end">
            <button
                    onClick={handleFormSubmit}
                    type="submit"
                    className="bg-pink-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                >
            Create
            </button>
            <button
                    type="button"
                    onClick={onClose}
                    className="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 ml-2 rounded"
                >
            Cancel
            </button>
            </div>
            </form>
            </div>
            </Modal>
            );
  }


export default CreateProduct;
