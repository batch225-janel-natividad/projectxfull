import React, { useState, useEffect } from "react";
import Cart from "./images/cart.jpg";
import Cart1 from "./images/cart1.jpg";
import Swal from 'sweetalert2'
import UserContext from "../UserContext";

function CardProduct(props) {
  const { product } = props;




  const [showModal, setShowModal] = useState(false);
  const [selectedProduct, setSelectedProduct] = useState();
  const [quantity, setQuantity] = useState(1);
  const [address, setAddress] = useState("");

  useEffect(() => {
    if (showModal) {
      fetch(`${process.env.REACT_APP_API_URL}/product/${product._id}`)
        .then((response) => response.json())
        .then((data) => {
          setSelectedProduct(data);
        })
        .catch((error) => {
          console.error("Error fetching product:", error);
        });
    }
  }, [showModal, product._id]);

  //BUY-NOW
  const [showPurchaseModal, setShowPurchaseModal] = useState(false);

  async function recordPurchase() {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/product/${product._id}`);
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      const selectedProduct = await response.json();
      const token = localStorage.getItem('token');
      if (!address) {
        throw new Error('Address is required');
      }
      const response2 = await fetch(`${process.env.REACT_APP_API_URL}/order/buy-now`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`
        },
        body: JSON.stringify({
          products: [{ productId: selectedProduct._id, quantity, price: selectedProduct.price }],
          address: address // add the address field to the request body
        })
      });
      if (!response2.ok) {
        throw new Error('Network response was not ok');
      }
      const data = await response2.json();
      console.log('Purchase recorded:', data);
      Swal.fire({
        icon: 'success',
        title: 'Purchase Successful',
        showConfirmButton: false,
        timer: 1500
      });
      window.location.reload();
    } catch (error) {
      console.error('There was a problem with the fetch operation:', error);
    }
  }

  return (
    <div className="bg-yellow-100 shadow-md rounded-md p-4 mt-10 mr-3 ml-3">
      <img src={Cart} className="w-full mb-4" />
      <h2 className="font-bold text-lg mb-2">{product.name}</h2>
      <p className="text-gray-700 mb-2"><div className="font-bold">Description:</div> {product.description}</p>
      <p className="text-gray-700 mb-2"><div className="font-bold">Categories:</div> {product.categories.join(", ")}</p>
      <p className="text-gray-700 mb-2"><div className="font-bold">Color:</div> {product.color}</p>
      <p className="text-gray-700 mb-2"><div className="font-bold">Size:</div> {product.size}</p>
      <p className="text-gray-700 mb-2 font-bold font-medium">Php {product.price}</p>
      <button
        className="bg-rose-300 hover:bg-pink-500 text-white font-bold py-2 px-2 rounded"
        onClick={() => setShowModal(true)}
      >
        View Details
      </button>
      {showModal && (
        <div className="fixed z-50 top-0 left-0 w-full h-full overflow-auto bg-gray-500 bg-opacity-75 flex justify-center items-center">
          <div className="bg-red-100 rounded-lg p-8 md:w-1/2 lg:w-1/3 xl:w-1/4 mt-20 mb-20">
            <h2 className="font-bold text-lg mb-2">
              {selectedProduct ? selectedProduct.name : "Loading..."}
            </h2>

            {selectedProduct ? (
              <>
                <img
                  src={Cart1}
                  alt={selectedProduct.name}
                  className="w-full mb-4"
                />
                <p className="text-gray-700 mb-2">
                  {selectedProduct.description}
                </p>
                <p className="text-gray-700 mb-2">
                  {selectedProduct.categories.join(", ")}
                </p>
                <p className="text-gray-700 mb-2">{selectedProduct.color}</p>
                <p className="text-gray-700 mb-2">{selectedProduct.size}</p>
                <p className="text-gray-700 mb-2 font-bold">Php {selectedProduct.price}</p>
                <button
                  className="bg-pink-500 hover:bg-red-100 text-white font-bold py-2 px-2 rounded"
                  onClick={() => setShowPurchaseModal(true)}
                >
                  Buy Now
                </button>

                {showPurchaseModal && (
                  <div className="fixed z-50 top-0 left-0 w-full h-full overflow-auto bg-gray-500 bg-opacity-75 flex justify-center items-center">
                    <div className="bg-red-100 rounded-lg p-8 md:w-1/2 lg:w-1/3 xl:w-1/4 mt-20 mb-20">
                      <h2 className="font-bold text-lg mb-2">{product.name}</h2>
                      <input
                        type="number"
                        min="1"
                        max="100"
                        value={quantity}
                        onChange={(e) => setQuantity(e.target.value)}
                        className="mb-2"
                      />
                      <input
                        type="text"
                        placeholder="Enter address"
                        value={address}
                        onChange={(e) => setAddress(e.target.value)}
                        className="mb-2"
                      />
                      <button
                        className="bg-pink-500 hover:bg-red-100 text-white font-bold py-2 px-2 rounded"
                        onClick={recordPurchase}
                      >
                        Confirm
                      </button>
                      <button
                        className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded ml-3"
                        onClick={() => setShowPurchaseModal(false)}
                      >
                        Cancel
                      </button>
                    </div>
                  </div>
                )}
              </>
            ) : (
              <div>Loading...</div>
            )}
            <button
              className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded ml-3"
              onClick={() => setShowModal(false)}
            >
              Close
            </button>
          </div>
        </div>


      )}
    </div>
  )
}



export default CardProduct;
