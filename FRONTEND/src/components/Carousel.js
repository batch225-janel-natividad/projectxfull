import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import React from 'react';
import image1 from './images/1.jpg';
import image2 from './images/2.jpg';
import image3 from './images/3.jpg';

import '../App.css';

const responsive = {
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 1,
    slidesToSlide: 1 // optional, default to 1.
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 1,
    slidesToSlide: 1 // optional, default to 1.
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
    slidesToSlide: 1 // optional, default to 1.
  }
};

const images = [image1, image2, image3];

const CarouselComponent = ({ deviceType }) => {
  return (

    <div className="my-4 md:my-10 lg:my-12 mx-auto w-500 h-85 bg-gray-200 p-2 rounded-lg ml-12 mr-12">
      <Carousel
        ssr={true}
        deviceType={deviceType}
        responsive={responsive}
        swipeable
        autoPlay
        autoPlaySpeed={400}
        infinite
        keyBoardControl
        customTransition="all .5"
        transitionDuration={300}
        removeArrowOnDeviceType={['tablet', 'mobile']}
      >
        {images.map((image, index) => (
          <img
            key={index}
            className="h-100 object-cover"
            src={image}
            alt={`Slide ${index + 1}`}
          />
        ))}
      </Carousel>
    </div>
  
  );
};

export default CarouselComponent;
