const router = require("express").Router();
const auth = require("../auth");
const cartController = require("../controllers/cartController");

// Add to cart
router.post("/add", auth.verify, (req, res) => {
  cartController.addToCart(req.body).then(result => res.send(result));
});

module.exports = router;
