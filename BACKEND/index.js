const express = require("express");
const app = express();
const mongoose = require("mongoose");
const dotenv = require("dotenv");
const cors = require('cors');

app.use(cors());

app.use(cors({
    origin: 'http://localhost:3000'
  }));

//ROUTER
const userRoute = require("./routes/userRoute");
const productRoute = require("./routes/productRoute");
const orderRoute = require("./routes/orderRoute");
const cartRoute = require("./routes/cartRoute");

dotenv.config();

mongoose
    .connect(process.env.MONGOOSE_URL)
    .then(() => console.log("Connection Successful"))
    .catch((error) => {
        console.log(error)
    });

const port = 3001;

app.use(express.json());
app.use("/user", userRoute);
app.use("/product", productRoute);
app.use("/order", orderRoute);
app.use("/cart", cartRoute);


app.listen(port, () =>
    console.log(`You are now connected at ${port}`));