const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema (
    {
        userId: {
            type: String,
            required: true
        },
        product: [
            {
                product: {
                    type: String
                },
                quantity: {
                    type: String,
                    default: 1
                },
            },
        ],
        totalAmount: {
            type: Number,
            required: true
        },
        address: {
            type: String,
            required: true
        },
        status: {
            type: String,
            default: "pending"
        },
        purchaseOn: {
            type: Date
        }
    },
    {timestamps: true}
);

module.exports = mongoose.model("Order", orderSchema);
