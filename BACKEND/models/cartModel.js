const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema(
    {
      userId: {
        type: String,
        required: true
      },
      product: [
        {
          productId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Product'
          },
          quantity: {
            type: Number,
            default: 1
          }
        }
      ]
    },
    { timestamps: true, strict: false }
  );
  


module.exports = mongoose.model("Cart", cartSchema);
