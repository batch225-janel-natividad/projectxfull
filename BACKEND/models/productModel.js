const mongoose = require("mongoose");

const productSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: true
        },
        description: {
            type: String,
            required: true
        },
        categories: {
            type: Array,
            required: true
        },
        color: {
            type: String,
            required: true
        },
        size: {
            type: String,
            required: true
        },
        price: {
            type: Number,
            required: true
        },
        isActive: {
            type: Boolean,
            required: true
        },
        createdOn: {
            type: Date
        },
    },
    {timestamp: true}
);

module.exports = mongoose.model("Product", productSchema);