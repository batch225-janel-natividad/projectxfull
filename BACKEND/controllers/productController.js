const User = require("../models/userModel");
const Product = require("../models/productModel");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const jwt = require("jsonwebtoken");

//Create Product
module.exports.createProd = (data) => {
	if(data.isAdmin){
		let new_product = new Product ({
			name: data.product.name,
			description: data.product.description,
            categories: data.product.categories,
            color: data.product.color,
            size: data.product.size,
            price: data.product.price,
            isActive: data.product.isActive,
		})

		return new_product.save().then((new_product, error) => {
			if(error){
				return false
			}
			return {
				message: 'New product successfully added!'
			}
		})
	} 

	let message = Promise.resolve('User is not Admin.')

	return message.then((value) => {
		return value
	})
		
};
// Retrive All Product
module.exports.retriveAllProd = async (product) => {

	const result = await Product.find({});
    return result;
};

// Retrieve All Active Product
module.exports.retrieveAllActive = async () => {
	const result = await Product.find({ isActive: true });
	return result;
  };

//Retrive Single Product
module.exports.retriveSingle = async (data) => {
	const product = await Product.findOne({...data, isActive: true});
	return product;
};

//Update Products
module.exports.updateProduct = (reqParams, reqBody) => {

	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		categories: reqBody.categories,
        color: reqBody.color,
        size: reqBody.size,
		price: reqBody.price,
        isActive: reqBody.isActive,
	};
		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product,error) => {
			if (error) {
				return false;
			} else {
				return {
					message: "Product updated successfully"
				}
			};
		});
};

//Archive Product
module.exports.archiveProduct = (reqParams) => {
	
	let updateActiveField = {
		isActive: false,

	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
		if (error) {
			return false;
		} else {
			return {
				message: "archiving product successfully"
			};
		};
	});
};

//Activate Product
module.exports.activateProduct = (reqParams) => {
	
	let updateActiveField = {
		isActive: true,

	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
		if (error) {
			return false;
		} else {
			return {
				message: "Activate product successfully"
			};
		};
	});
};